package sk.bosacky.thequickandthedead.rest

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.mockito.InjectMock
import io.restassured.RestAssured
import org.hamcrest.CoreMatchers
import org.junit.jupiter.api.Test
import sk.bosacky.thequickandthedead.db.WorkoutRepository

@QuarkusTest
class WorkoutResourceTest {
    @InjectMock
    lateinit var workoutRepository: WorkoutRepository

    @Test
    fun testRandomEndpoint() {
        RestAssured.given()
                .`when`()["/workouts/random"]
                .then()
                .statusCode(200)
                .body(CoreMatchers.containsString("numberOfSets"))
    }
}
