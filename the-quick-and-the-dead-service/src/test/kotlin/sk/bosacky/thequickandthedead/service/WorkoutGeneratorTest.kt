package sk.bosacky.thequickandthedead.service

import io.quarkus.test.junit.QuarkusTest
import org.hamcrest.MatcherAssert
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import sk.bosacky.thequickandthedead.db.WorkoutType
import javax.inject.Inject

@QuarkusTest
class WorkoutGeneratorTest {

    @Inject
    lateinit var workoutGenerator: WorkoutGenerator

    @Test
    fun testRandomizeFromEnum() {
        val actual = workoutGenerator.randomizeFromEnum(WorkoutType::class)
        MatcherAssert.assertThat("", actual, Matchers.`in`(listOf(WorkoutType.SNATCHES, WorkoutType.SWINGS_AND_PUSHUPS)))
    }

}
