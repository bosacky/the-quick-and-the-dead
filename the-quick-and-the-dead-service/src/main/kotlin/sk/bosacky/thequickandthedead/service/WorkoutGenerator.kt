package sk.bosacky.thequickandthedead.service

import sk.bosacky.thequickandthedead.db.*
import java.time.LocalDate
import java.util.*
import java.util.concurrent.ThreadLocalRandom
import javax.enterprise.context.ApplicationScoped
import kotlin.reflect.KClass

interface WorkoutGenerator {
    fun randomizeWorkout(randomizeWorkoutType: Boolean = false, randomizeTrainingDays: Boolean = false): Optional<Workout>

    fun <T : Enum<T>> randomizeFromEnum(klass: KClass<out Enum<T>>): T
}

@ApplicationScoped
class WorkoutGeneratorImpl : WorkoutGenerator {

    override fun randomizeWorkout(randomizeWorkoutType: Boolean, randomizeTrainingDays: Boolean): Optional<Workout> {
        //TODO:
        val kettlebellWeightKg = 40
        val band = Band.None

        val workoutType = if (randomizeWorkoutType) randomizeFromEnum(WorkoutType::class) else WorkoutType.SWINGS_AND_PUSHUPS
        val numberOfSets = randomizeNumberOfSeries()
        val repsSets = randomizeFromEnum(RepsSets::class)

        val isTrainingDay = !randomizeTrainingDays || ThreadLocalRandom.current().nextBoolean()

        return if (isTrainingDay)
            when (workoutType) {
                WorkoutType.SWINGS_AND_PUSHUPS ->
                    Optional.of(WorkoutFactory.swingsAndPushupsWorkout(
                            LocalDate.now(),
                            repsSets,
                            numberOfSets,
                            randomizeFromEnum(SwingType::class),
                            randomizeFromEnum(PushupType::class),
                            kettlebellWeightKg,
                            band))
                WorkoutType.SNATCHES -> {
                    Optional.of(WorkoutFactory.snatchesWorkout(
                            LocalDate.now(),
                            repsSets,
                            numberOfSets,
                            kettlebellWeightKg,
                            band))
                }
            } else Optional.empty()
    }

    override fun <T : Enum<T>> randomizeFromEnum(klass: KClass<out Enum<T>>): T {
        val values = klass.java.enumConstants.filterNot { "N_A" == it.name }
        return values[ThreadLocalRandom.current().nextInt(values.size)] as T
    }

    fun <T : Enum<T>> iterator(values: () -> Array<T>): Iterator<T> = values()
            .asIterable()
            .iterator()

    private fun randomizeNumberOfSeries(): Int {
        return when (val roll = ThreadLocalRandom.current().nextInt(6) + 1) {
            1 -> 2
            2, 3 -> 3
            4, 5 -> 4
            6 -> 5
            else -> throw IllegalStateException("Unexpected value: $roll")
        }
    }
}
