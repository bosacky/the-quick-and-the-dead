package sk.bosacky.thequickandthedead.rest

import io.quarkus.mongodb.panache.reactive.ReactivePanacheQuery
import io.quarkus.panache.common.Page
import io.smallrye.mutiny.Uni
import sk.bosacky.thequickandthedead.db.Workout
import sk.bosacky.thequickandthedead.service.WorkoutGenerator
import sk.bosacky.thequickandthedead.db.WorkoutRepository
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response


@Path("/workouts")
class WorkoutResource {
    @Inject
    lateinit var workoutGenerator: WorkoutGenerator

    @Inject
    lateinit var workoutRepository: WorkoutRepository

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    fun getAll(@BeanParam pageRequest: PageRequest): Response {
        val page: Page = Page.of(pageRequest.pageNum, pageRequest.pageSize)
        val query: ReactivePanacheQuery<Workout> = workoutRepository.findAll().page(page)
        val workouts = query.list<Workout>().await().indefinitely()
        return Response.ok(workouts).build()
    }

    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    fun addWorkout(workout: Workout): Uni<Void> =
            //TODO: return hateoas link
            workoutRepository.persist(workout)

    @GET
    @Path("/random")
    @Produces(MediaType.APPLICATION_JSON)
    fun getRandom(
            @QueryParam("randomizeWorkoutType") randomizeWorkoutType: Boolean,
            @QueryParam("randomizeTrainingDays") randomizeTrainingDays: Boolean): Uni<Workout> =
            Uni.createFrom().optional(workoutGenerator.randomizeWorkout(randomizeWorkoutType, randomizeTrainingDays))

}
