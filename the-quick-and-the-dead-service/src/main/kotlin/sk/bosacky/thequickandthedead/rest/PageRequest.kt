package sk.bosacky.thequickandthedead.rest

import javax.ws.rs.DefaultValue
import javax.ws.rs.QueryParam

class PageRequest {
        @QueryParam("pageNum")
        @DefaultValue("0")
        var pageNum = 0

        @QueryParam("pageSize")
        @DefaultValue("10")
        var pageSize = 0
}
