package sk.bosacky.thequickandthedead.db

enum class SwingType {
    TWO_ARM, ONE_ARM, N_A;
}

enum class PushupType {
    PALMS, FISTS, N_A;
}

enum class RepsSets {
    FIVE_FOUR, ALTERNATE, TEN_TWO;
}

enum class WorkoutType {
    SWINGS_AND_PUSHUPS, SNATCHES;
}

enum class Band(val resistance: String) {
    None("N/A"),
    Tan("XX-Light"),
    Yellow("X-Light"),
    Red("Light"),
    Green("Medium"),
    Blue("Heavy"),
    Black("X-Heavy"),
    Silver("XX-Heavy"),
    Gold("XXX-Heavy");
}
