package sk.bosacky.thequickandthedead.db

import com.fasterxml.jackson.annotation.JsonIgnore
import io.quarkus.mongodb.panache.MongoEntity
import io.quarkus.mongodb.panache.PanacheMongoEntityBase
import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoRepository
import org.bson.codecs.pojo.annotations.BsonCreator
import org.bson.codecs.pojo.annotations.BsonId
import org.bson.codecs.pojo.annotations.BsonIgnore
import org.bson.codecs.pojo.annotations.BsonProperty
import org.bson.types.ObjectId
import java.time.Duration
import java.time.LocalDate
import javax.enterprise.context.ApplicationScoped

@MongoEntity(collection = "workouts")
data class Workout @BsonCreator constructor(
        @BsonProperty("date") val date: LocalDate,
        @BsonProperty("workoutType") val workoutType: WorkoutType,
        @BsonProperty("repsSets") val repsSets: RepsSets,
        @BsonProperty("numberOfSets") val numberOfSets: Int,
        @BsonProperty("setDuration") val setDuration: Duration,
        @BsonProperty("swingType") val swingType: SwingType,
        @BsonProperty("pushupType") val pushupType: PushupType,
        @BsonProperty("kettlebellWeightKg") val kettlebellWeightKg: Int,
        @BsonProperty("band") val band: Band,
        @BsonId @JsonIgnore val id: ObjectId = ObjectId()
) : PanacheMongoEntityBase() {
    val totalReps: Int = 20 * numberOfSets
    val totalDuration: Duration = setDuration.multipliedBy(numberOfSets.toLong())
}

object WorkoutFactory {

    @BsonIgnore
    private const val SWING_AND_PUSHUPS_SET_DURATION = 6L

    @BsonIgnore
    private const val SNATCHES_SET_DURATION = 4L

    fun swingsAndPushupsWorkout(date: LocalDate, repsSets: RepsSets, numberOfSets: Int, swingType: SwingType, pushupType: PushupType, kettlebellWeightKg: Int, band: Band) = Workout(
            date, WorkoutType.SWINGS_AND_PUSHUPS, repsSets, numberOfSets,
            Duration.ofMinutes(SWING_AND_PUSHUPS_SET_DURATION), swingType, pushupType, kettlebellWeightKg, band)

    fun snatchesWorkout(date: LocalDate, repsSets: RepsSets, numberOfSets: Int, kettlebellWeightKg: Int, band: Band) = Workout(
            date, WorkoutType.SNATCHES, repsSets, numberOfSets,
            Duration.ofMinutes(SNATCHES_SET_DURATION), SwingType.N_A, PushupType.N_A, kettlebellWeightKg, band)
}

@ApplicationScoped
class WorkoutRepository : ReactivePanacheMongoRepository<Workout> {

}
