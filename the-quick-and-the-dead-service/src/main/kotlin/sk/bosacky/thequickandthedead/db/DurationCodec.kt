package sk.bosacky.thequickandthedead.db

import com.mongodb.MongoClientSettings
import org.bson.BsonReader
import org.bson.BsonString
import org.bson.BsonWriter
import org.bson.codecs.Codec
import org.bson.codecs.DecoderContext
import org.bson.codecs.EncoderContext
import org.bson.codecs.configuration.CodecProvider
import org.bson.codecs.configuration.CodecRegistry
import java.time.Duration

class DurationCodec : Codec<Duration> {
    private val documentCodec: Codec<BsonString> = MongoClientSettings.getDefaultCodecRegistry().get(BsonString::class.java)

    override fun encode(writer: BsonWriter?, duration: Duration?, context: EncoderContext?) {
        val str = BsonString(duration.toString())
        documentCodec.encode(writer, str, context)
    }

    override fun getEncoderClass(): Class<Duration> {
        return Duration::class.java
    }

    override fun decode(reader: BsonReader?, decoderContext: DecoderContext?): Duration {
        val str: BsonString = documentCodec.decode(reader, decoderContext)
        return Duration.parse(str.value)
    }

}

class DurationCodecProvider : CodecProvider {
    override fun <T> get(clazz: Class<T>, registry: CodecRegistry): Codec<T>? {
        return if (clazz == Duration::class.java) {
            DurationCodec() as Codec<T>
        } else null
    }
}
