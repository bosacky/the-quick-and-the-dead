export interface Workout {
  numberOfSets: number;
  repsSets: string;
  totalReps: number;
  totalDuration: string;
  workoutType: string;
  setDuration: string;
  swingType: string;
  pushupType: string;
}
