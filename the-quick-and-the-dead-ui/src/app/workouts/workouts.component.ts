import {Component, OnInit} from '@angular/core';
import {Workout} from '../workout';
import {WorkoutService} from '../workout.service';

@Component({
  selector: 'app-workouts',
  templateUrl: './workouts.component.html',
  styleUrls: ['./workouts.component.scss']
})
export class WorkoutsComponent implements OnInit {

  workouts: Workout[] = [];
  selectedWorkout: Workout;

  constructor(private workoutService: WorkoutService) {
  }

  ngOnInit(): void {
  }

  public getRandomWorkout(): void {
    this.workoutService.getRandomWorkout().subscribe(workout => {
      this.workouts.push(workout);
    });
  }

  onSelect(workout: Workout): void {
    this.selectedWorkout = workout;
  }
}
