import {Injectable} from '@angular/core';
import {Workout} from './workout';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MessageService} from './message.service';
import {environment as env} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WorkoutService {

  private workoutsUrl = `${env.service_endpoint}/workouts`;  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'access-control-allow-origin': `${env.service_endpoint}`
    })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) {
  }

  /** GET workout by id. Will 404 if id not found */
  getRandomWorkout(): Observable<Workout> {
    const url = `${this.workoutsUrl}/random`;
    return this.http.get<Workout>(url).pipe(
      tap(_ => this.log(`fetched random workout`)),
      catchError(this.handleError<Workout>(`error getting random workout`))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T): any {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string): void {
    this.messageService.add(`WorkoutService: ${message}`);
  }
}
